Elasticsearch Cluster
-----------

These IAC's will provision the ec2 instance and setup elastic search cluster.



## [Requirements](id:requirements)


- Ansible 2.5+

- Expects CentOS/RHEL, Ubuntu, Amazon Linux or SLES hosts

- Terraform 0.14.0



## [Parts](id:parts)

These IAC's are majorly divided into 2 parts.

1. Provisioning (Terraform): This will create the VPC, Subnets, Subgroups, Internet Gateways, SSH keys, EC2 Instances etc.

2. Configurating (Ansible): This will configure the ec2 instance and setup elastic cluster with TLS and HTTPS security.

# Setup the AWS credentials file


1. Get the AWS access key and secret

   Decide on the account you want to use for the purpose of these scripts or create a new one in IAM (with a `PowerUserAccess` policy attached to it).
   
   [Create](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey) an Access Key if none is present.
   
   Obtain the `Access Key ID` and the `Secret Access Key`.


2. Export the environment variables

   With the Access Key details obtained, export them as environment variables:
 
   ```
   export TF_VAR_aws_access_key='AK123'
   export TF_VAR_aws_secret_key='abc123'
   ```

# Clone the repository

Upload the ansible-hortonworks repository to the build node / workstation, preferable under the home folder.

If the build node / workstation can directly download the repository, run the following:

```
cd && git clone https://gitlab.com/gursimran.singh1/elastic-cluster.git
```

# Set the Ansible variables

   ```
   export ANSIBLE_HOST_KEY_CHECKING=false
   ```

## cloud_config

This section contains variables that are cluster specific and are used by all nodes:

| Variable            | Description                                                                                             |
| ------------------- | ------------------------------------------------------------------------------------------------------- |
| region              | The AWS Region as described [here](https://docs.aws.amazon.com/general/latest/gr/rande.html#ec2_region). |
| zone                | The AWS Availability Zone from the previously set Region. More details [here](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#using-regions-availability-zones-describe). |
| vpc_name / vpc_cidr | The Amazon Virtual Private Cloud as described [here](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Introduction.html). It will be created if it doesn't exist. The name and the CIDR uniquely identify a VPC so set these variables accordingly if you want to build in an existing VPC. |
| subnet_cidr         | Subnet is a range of IP addresses in the VPC as described [here](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html). |
| internet_gateway    | Set this to `true` if the VPC has an Internet Gateway. Without one, the cluster nodes cannot reach the Internet (useful to download packages) so only set this to `false` if the nodes will use repositories located in the same VPC. More details about Internet Gateways [here](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Internet_Gateway.html). |
| admin_username      | The Linux user with sudo permissions. Usually this is `ec2-user`.                                          |
| ssh`.keyname`       | The name of the AWS SSH key that will be placed on cluster nodes at build time. Can be an existing one otherwise a new key will be uploaded. |
| ssh`.privatekey`    | Local path to the SSH private key that will be used to login into the nodes. This can be the key generated as part of the Build Setup, step 5. |
| ssh`.publickey`     | Local path to the SSH public key that will be placed on cluster nodes at build time. This public key will be uploaded to AWS if one doesn't exist. |
| security_groups     | A list of Access Control List (ACL) associated with the subnet. Details [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html#vpc-security-groups). By default, nodes in the same security group are not allowed to communicate to each other unless there is a rule to allow traffic originating from the same group. This rule is defined in the `default_cluster_access` security group and should be kept as it is. |



# Build the Cloud environment

Run the script that will build the Cloud environment.

```
terraform -chdir=provisioning init
terraform -chdir=provisioning apply
```

# Install the cluster

Run the script that will install the cluster.

```
ansible-playbook configuration/ec2_elastic_playbook.yml -i hosts --key-file elasticsearch.pem
```



1.Permissions for service account:

AmazonVPCFullAccess
AmazonEC2FullAccess
AmazonIAMFullAccess(To create role and policies(list/access other ec2 instances))

2.Set Variable for terraform

export TF_VAR_aws_access_key= access_key
export TF_VAR_aws_secret_key= secret_key
export ANSIBLE_HOST_KEY_CHECKING=false


3. go to the root directory of the folder and type following commands

terraform -chdir=provisioning init
terraform -chdir=provisioning apply

ansible-playbook configuration/ec2_elastic_playbook.yml -i hosts --key-file elasticsearch.pem


4. Username and password are randomly generate and to vberify the installation ssh into any node and run this command

curl -k -XGET -u USERNAME:PASSWORD 'https://localhost:9200/_cluster/health'

