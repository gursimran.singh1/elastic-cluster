variable "ami" {
  type = "map"

  default = {
    "us-east-1" = "ami-0742b4e673072066f"
  }
}

variable "instance_count" {
  default = "1"
}

variable "key_name" {
  default = "elasticsearch"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "aws_access_key" {
}

variable "aws_secret_key" {
}

variable "aws_region" {
     default = "us-east-1"
}

variable "availabilityZone" {
     default = "us-east-1a"
}

variable "instanceTenancy" {
    default = "default"
}

variable "dnsSupport" {
    default = true
}

variable "dnsHostNames" {
    default = true
}

variable "vpcCIDRblock" {
    default = "10.0.0.0/24"
}
variable "publicSubnetCIDRblock" {
    default = "10.0.0.0/28"
}

variable "mapPublicIP" {
    default = false
}
