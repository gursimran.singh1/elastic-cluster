resource "aws_vpc" "elastic-vpc" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy 
  enable_dns_support   = var.dnsSupport 
  enable_dns_hostnames = var.dnsHostNames
tags = {
    Name = "Elastic VPC"
}
} 

resource "aws_subnet" "public-subnet" {
  vpc_id                  = aws_vpc.elastic-vpc.id
  cidr_block              = var.publicSubnetCIDRblock
  map_public_ip_on_launch = var.mapPublicIP 
  availability_zone       = var.availabilityZone
tags = {
   Name = "Elastic Subnet"
}
}

resource "aws_security_group" "elastic-sg" {
  vpc_id      = aws_vpc.elastic-vpc.id
  name        = "elastic-sg"
  description = "security group that allows ssh and all egress traffic"

  tags = {
    Name = "elastic-sg"
  }
}

resource "aws_security_group_rule" "ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.elastic-sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "all-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.elastic-sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "private-traffic" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = aws_security_group.elastic-sg.id
  source_security_group_id = aws_security_group.elastic-sg.id
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.elastic-vpc.id

  tags = {
    Name = "elastic"
  }
}

resource "aws_route" "all" {
  route_table_id              = aws_vpc.elastic-vpc.default_route_table_id 
  destination_cidr_block = "0.0.0.0/0"
  gateway_id      = aws_internet_gateway.gw.id
}