resource "aws_instance" "cluster_member" {
  count         = "${var.instance_count}"
  ami           = "${lookup(var.ami,var.aws_region)}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  vpc_security_group_ids = [aws_security_group.elastic-sg.id]
  subnet_id = aws_subnet.public-subnet.id
  associate_public_ip_address = true
  iam_instance_profile = "${aws_iam_instance_profile.elastic_profile.name}"

  tags = {
    env  = "elastic"
    purpose = "demo"
  }
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "elastic" {
  key_name   = "${var.key_name}"     
  public_key = tls_private_key.ssh.public_key_openssh
}

resource "local_file" "pem_file" {
  filename = pathexpand("${path.module}/../elasticsearch.pem")
  file_permission = "400"
  directory_permission = "700"
  sensitive_content = tls_private_key.ssh.private_key_pem
}

output "instance_public_ip" {
    value = "${aws_instance.cluster_member[0].public_ip}"
}

output "instance_key" {
    value = "${aws_instance.cluster_member[0].key_name}"
}

output "instance_id" {
    value = "${aws_instance.cluster_member[0].id}"
}