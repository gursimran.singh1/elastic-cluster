resource "local_file" "provision_cluster_member_hosts_file" {
    content     = "${join("\n", formatlist("%v", concat(["[master]"],[aws_instance.cluster_member.*.public_ip[0]],["[elastic_demo]"],aws_instance.cluster_member.*.public_ip,["[elastic_demo:vars]"],[join("",["master=",aws_instance.cluster_member.*.private_ip[0]])])))}"
    filename = "${path.module}/../hosts"
}
