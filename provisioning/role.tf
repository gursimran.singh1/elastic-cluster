resource "aws_iam_role" "elastic" {
  name = "elastic"

  assume_role_policy = jsonencode({
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "ec2.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
})
}


resource "aws_iam_policy" "elastic-cluster" {
  name        = "elastic-cluster"
  description = "Read Only Policy"
  policy      = jsonencode(
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        }
    ]
})
}


resource "aws_iam_policy_attachment" "ec2-role-policy-attachment" {
  name       = "test-attachment"
  roles      = ["${aws_iam_role.elastic.name}"]
  policy_arn = "${aws_iam_policy.elastic-cluster.arn}"
}

resource "aws_iam_instance_profile" "elastic_profile" {
  name  = "elastic_profile"
  role = "${aws_iam_role.elastic.name}"
}