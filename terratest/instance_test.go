package test
import (
   "testing"
//    "fmt"
//    "time"
   "github.com/stretchr/testify/assert"
   "github.com/gruntwork-io/terratest/modules/terraform"
   "github.com/gruntwork-io/terratest/modules/aws"
//    "github.com/gruntwork-io/terratest/modules/ssh"
//    "github.com/gruntwork-io/terratest/modules/retry"
)


func TestInstancePublicIp(t *testing.T) {
    terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../provisioning",
	})
    defer terraform.Destroy(t, terraformOptions)
    terraform.InitAndApply(t, terraformOptions)
    instanceIP := terraform.Output(t, terraformOptions, "instance_public_ip")
    instanceID := terraform.Output(t, terraformOptions, "instance_id")
    instanceIPFromInstance := aws.GetPublicIpOfEc2Instance(t, instanceID, "us-east-1")
    assert.Equal(t, instanceIP, instanceIPFromInstance)
}